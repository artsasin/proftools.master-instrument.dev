<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use AppBundle\Manager\ImportManager;

class ProftoolsYmlImportCommand extends ContainerAwareCommand
{
	/**
	 * (non-PHPdoc)
	 * @see \Symfony\Component\Console\Command\Command::configure()
	 */
	protected function configure() {
		$this->setName('proftools:import:yml')
			->addOption('priceOnly', null, InputOption::VALUE_NONE, 'If set, the task will import only prices')
			->addOption('noDownload', null, InputOption::VALUE_NONE, 'If set, the task will not download file and use existing one')
		;
	}
	
	final protected function execute(InputInterface $input, OutputInterface $output) {
		$priceOnly = $input->getOption('priceOnly');
		$noDownload = $input->getOption('noDownload'); 
		$importManager = $this->getImportManager();
		$importManager->setOutput($output);
		$importManager->setUp();
		$importManager->import($priceOnly, $noDownload);
	}
	
	/**
	 * @return ImportManager
	 */
	private function getImportManager()
	{
		return $this->getContainer()->get('import.manager');
	}
}