<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{
	public function findAllAssoc() {
		$qb = $this->getEntityManager()->createQueryBuilder();
		$qb->select('p');
		$qb->from($this->getClassName(), 'p', 'p.externalId');
		
		return $qb->getQuery()->getResult();
	}
	
	/**
	 * @param number $count
	 */
	public function getRandomEntities($count) {
		return $this->createQueryBuilder('p')
			->addSelect('RAND() as HIDDEN rand')
			->addOrderBy('rand')
			->setMaxResults($count)
			->getQuery()
			->getResult();
	}
}