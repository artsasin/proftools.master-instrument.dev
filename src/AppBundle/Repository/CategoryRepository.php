<?php

namespace AppBundle\Repository;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository as TreeEntityRepository;

class CategoryRepository extends TreeEntityRepository
{
	public function findAllAssoc() {
		$qb = $this->getEntityManager()->createQueryBuilder();
		$qb->select('c');
		$qb->from($this->getClassName(), 'c', 'c.externalId');
		
		return $qb->getQuery()->getResult();
	}
}