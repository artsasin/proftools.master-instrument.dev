<?php

namespace AppBundle\Manager;

use Doctrine\ORM\EntityManagerInterface as ObjectManager;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository as EntityRepository;
use AppBundle\Entity\Category;
use AppBundle\Repository\CategoryRepository;

class CategoryManager
{
	/**
	 * @var ObjectManager
	 */
	private $em;
	
	/**
	 * @var CategoryRepository
	 */
	private $repository;
	
	/**
	 * @var array
	 */
	private $cache;
	
	public function __construct(ObjectManager $em) {
		$this->em = $em;
		$this->repository = $em->getRepository('AppBundle:Category');
		$this->cache = $this->repository->findAllAssoc();
	}
	
	/**
	 * @param string $externalId
	 * @return Category
	 */
	public function createOrFindFromArray($externalId) {
		if (array_key_exists($externalId, $this->cache)) {
			$c = $this->em->merge($this->cache[$externalId]);
		} else {
			$c = new Category();
			$this->cache[$externalId] = $c;
			$this->em->persist($c);
		}
		return $c;
	}
	
	/**
	 * @param string $externalId
	 * @return Category
	 */
	public function getByExternalId($externalId) {
		return $this->repository->findOneBy(array('externalId' => $externalId));
	}
	
	public function getHtmlTree() {
		return $this->repository->childrenHierarchy();
	}
	
	/**
	 * @param int $categoryId
	 * @return Category
	 */
	public function getCategoryById($categoryId) {
		return $this->repository->find($categoryId);
	}
}