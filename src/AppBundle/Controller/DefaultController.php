<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use AppBundle\Manager\CategoryManager;
use AppBundle\Manager\ProductManager;
use AppBundle\Model\ContactFormModel;
use AppBundle\Form\ContactFormType;

class DefaultController extends Controller
{
    /**
	 * @return CategoryManager
	 */
	private function getCategoryManager() {
		return $this->get('category.manager');
	}
	
	/**
	 * @return ProductManager
	 */
	private function getProductManager() {
		return $this->get('product.manager');
	}
	
    /**
     * @Route("/", name="homepage")
     * @Template()
     */
    public function indexAction(Request $request)
    {
    	$cm = $this->getCategoryManager();
    	$pm= $this->getProductManager();
    	$htmlTree = $cm->getHtmlTree();
    	$randomProducts = $pm->getRandomProducts();
        return array(
        		'htmlTree' => $htmlTree,
        		'randomProducts' => $randomProducts,
        );
    }
    
    /**
     * @Route("/category/{categoryId}", name="view_category")
     * @Template()
     * @param int $categoryId
     */
    public function viewCategoryAction($categoryId) {
    	$cm = $this->getCategoryManager();
    	//$pm= $this->getProductManager();
    	$htmlTree = $cm->getHtmlTree();
    	$category = $cm->getCategoryById($categoryId);
    	if (!$category) {
    		throw new NotFoundHttpException("Запрошенная категория не найдена на нашем сайте!");
    	}
    	//$categoryProducts = $pm->getProductsFromCategory($category);
    	$categoryProducts = $category->getProducts();
    	return array(
    			'categoryProducts' => $categoryProducts,
    			'htmlTree' => $htmlTree,
    			'category' => $category,
    	);
    }
    
    /**
     * @Route("/product/{productId}", name="view_product")
     * @Template()
     * @param int $productId
     */
    public function viewProductAction($productId) {
    	$cm = $this->getCategoryManager();
    	$pm= $this->getProductManager();
    	$htmlTree = $cm->getHtmlTree();
    	$product = $pm->getProductById($productId);
    	$category = $product->getCategories()->first();
    	if (!$category || !$product) {
    		throw new NotFoundHttpException("Запрошенная информация не найдена на нашем сайте!");
    	}
    	return array(
    			'htmlTree' => $htmlTree,
    			'product' => $product,
    			'category' => $category,
    	);
    }
    
    /**
     * @Route("/contacts", name="view_contacts")
     * @Template()
     * @param Request $request
     */
    public function contactsAction(Request $request) {
    	$contactFormModel = new ContactFormModel();
    	$form = $this->createForm(new ContactFormType(), $contactFormModel);
    	if ($request->isMethod("POST")) {
    		$form->handleRequest($request);
    		if ($form->isValid()) {
    			// send email and clear form data
    			$message = \Swift_Message::newInstance()
    			->setSubject('Обратная связь с сайта hanstools: '.$contactFormModel->getSubject())
    			->setFrom($this->getParameter('mail_from'))
    			->setTo($this->getParameter('mail_admin'))
    			->setBody(
    					$this->renderView(
    							// app/Resources/views/Emails/registration.html.twig
    							'AppBundle::contactForm_email.html.twig',
    							array('contactFormModel' => $contactFormModel)
    							),
    					'text/html'
    					)
    			;
    			$this->get('mailer')->send($message);
    			$this->addFlash('success', "Ваше сообщение отправлено");
    			$contactFormModel = new ContactFormModel();
    			$form = $this->createForm(new ContactFormType(), $contactFormModel);
    		} else {
    			$this->addFlash('error', "Ошибка отправки формы");
    		}
    	}
    	
    	return array('form' => $form->createView());
    }
}
