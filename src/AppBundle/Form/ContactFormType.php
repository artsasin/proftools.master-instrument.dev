<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactFormType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{		
		$builder
		->add('name', 'text', array('label' => false, "required" => true))
		->add('email', 'email', array('label' => false, "required" => true))
		->add('subject', 'text', array('label' => false, "required" => true))
		->add('message', 'textarea', array('label' => false, "required" => true))
		;
	}
	
	public function getName()
	{
		return 'contact_form';
	}
	
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'AppBundle\Model\ContactFormModel',
		));
	}
}