<?php

namespace AppBundle\Manager;

use Doctrine\ORM\EntityManagerInterface as ObjectManager;
use AppBundle\Entity\Product;
use AppBundle\Repository\ProductRepository;
use AppBundle\Entity\Category;

class ProductManager
{
	/**
	 * @var ObjectManager
	 */
	private $em;

	/**
	 * @var ProductRepository
	 */
	private $repository;

	/**
	 * @var array
	 */
	private $cache;

	public function __construct(ObjectManager $em) {
		$this->em = $em;
		$this->repository = $em->getRepository('AppBundle:Product');
		$this->cache = $this->repository->findAllAssoc();
	}

	/**
	 * @param string $externalId
	 * @return Product
	 */
	public function createOrFindFromArray($externalId) {
		if (array_key_exists($externalId, $this->cache)) {
			$p = $this->em->merge($this->cache[$externalId]);
		} else {
			$p = new Product();
			$this->cache[$externalId] = $p;
			$this->em->persist($p);
		}
		return $p;
	}
	
	/**
	 * @param number $count
	 */
	public function getRandomProducts($count = 9) {
		return $this->repository->getRandomEntities($count);
	}
	
	/**
	 * @param string $guid
	 * @return Product
	 */
	public function getProductByExternalId($externalId) {
		return $this->repository->findOneBy(array('externalId' => $externalId));
	}
	
	/**
	 * @return Product
	 * @param int $productId
	 */
	public function getProductById($productId) {
		return $this->repository->find($productId);
	}
}