<?php

namespace AppBundle\Manager;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DomCrawler\Crawler;

use AppBundle\Entity\Category;

/**
 * 
 * @author artsasin
 *
 */
class ImportManager extends ContainerAware
{
	const YML_URL = "http://www.master-instrument.ru/bitrix/catalog_export/yandex_torg.php";
	
	/**
	 * @var OutputInterface
	 */
	private $output;
	
	/**
	 * @var ProductManager
	 */
	private $productManager;
	
	/**
	 * @var CategoryManager
	 */
	private $categoryManager;
	
	/**
	 * @var EntityManager
	 */
	private $em;
	
	/**
	 * @var Filesystem
	 */
	private $fs;
	
	/**
	 * @param ConsoleOutput $output
	 */
	public function setOutput(OutputInterface $output)
	{
		$this->output = $output;
		
		return $this;
	}
	
	public function setUp()
	{
		$this->fs = new Filesystem();
		
		$this->productManager = $this->container->get('product.manager');		
		$this->categoryManager = $this->container->get('category.manager');
		
		$this->em = $this->container->get('doctrine')->getManager();
		$this->em->getConnection()->getConfiguration()->setSQLLogger(null);
	}
	
	/**
	 * @param bool $priceOnly
	 * @param bool $noDownload
	 */
	public function import($priceOnly = false, $noDownload = false)
	{		
		$this->output->writeln('<info>Начинаю импорт товаров из YML</info>');
		$crawler = new Crawler(file_get_contents(self::YML_URL));
		$categories = $crawler->filter('yml_catalog shop categories category')->each(function(Crawler $node) {
			return [
					'title'				=> $node->text(),
					'externalId'		=> $node->attr('id'),
					'parentExternalId'	=> $node->attr('parentId')
			];
		});
		
		$offers = $crawler->filter('yml_catalog shop offers offer')->each(function(Crawler $node) {
			$categories = $node->filter('categoryId')->each(function(Crawler $cnode) {
				return $cnode->text();
			});
			$oldPrice = null;
			if ($node->filter('oldprice')->count() > 0) {
				$oldPrice = $node->filter('oldprice')->text();
			}
			return [
					'externalId'	=> $node->attr('id'),
					'active'		=> boolval($node->attr('available')),
					'name'			=> ($node->filter('name')->count() > 0 ? $node->filter('name')->text() : null),
					'url'			=> ($node->filter('url')->count() > 0 ? $node->filter('url')->text() : null),
					'price'			=> ($node->filter('price')->count() > 0 ? $node->filter('price')->text() : null),
					'oldPrice'		=> ($node->filter('oldprice')->count() > 0 ? $node->filter('oldprice')->text() : null),
					'categories'	=> $categories,
					'picture'		=> ($node->filter('picture')->count() > 0 ? $node->filter('picture')->text() : null),
					'description'	=> ($node->filter('description')->count() > 0 ? $node->filter('description')->text() : null),
					'sales_notes'	=> ($node->filter('sales_notes')->count() > 0 ? $node->filter('sales_notes')->text() : null),
			];
		});
		//print_r($offers);
		//print_r($categories);
		$this->importCategories($categories);
		
		$this->importOffers($offers);
		
		$this->output->writeln('<info>Импорт окончен</info>');
	}
	
	protected function isSuccessful($statusCode)
	{
		return $statusCode >= 200 && $statusCode < 300;
	}
	
	/**
	 * @param Array() $categories
	 */
	private function importCategories($categories)
	{
		$this->output->writeln("\tКатегорий для импорта: ".count($categories));
		foreach ($categories as $item) {
			$externalId = $item['externalId'];
			$parentExternalId = $item['parentExternalId'];
			$title = $item['title'];
			$this->output->writeln(sprintf("\t\tИмпорт категории: %s [%s] < [%s]", $title, $externalId, $parentExternalId));
			
			$c = $this->categoryManager->createOrFindFromArray($externalId);
			$pc = null;
			if ($parentExternalId) {
				$pc = $this->categoryManager->createOrFindFromArray($parentExternalId);
			}
			$c->setParent($pc);
			$c->setTitle($title);
			$c->setExternalId($externalId);
		}
		
		$this->em->flush();
		$this->em->clear();
		$this->output->writeln("\tОкончание импорта категорий");
	}
	
	/**
	 * @param array $offers
	 */
	private function importOffers($offers) {
		$this->output->writeln("\tТоваров для импорта: ".count($offers));
		foreach ($offers as $item) {
			if ($item['name']) {
				$this->output->writeln(sprintf("\t\tИмпорт товара: %s [%s]", $item['name'], $item['externalId']));
				$p = $this->productManager->createOrFindFromArray($item['externalId']);
				$p->setTitle($item['name']);
				$p->setDescription($item['description']);
				$p->setExternalId($item['externalId']);
				$p->setImage($item['picture']);
				$p->setIsActive($item['active']);
				$p->setOldPrice($item['oldPrice']);
				$p->setPrice($item['price']);
				foreach ($item['categories'] as $categoryExternalId) {
					$c = $this->categoryManager->getByExternalId($categoryExternalId);
					if ($c && !$p->getCategories()->contains($c)) {
						$p->addCategory($c);
					}
				}
			}
		}
		
		$this->em->flush();
		$this->em->clear();
		$this->output->writeln("\tОкончание импорта товаров");
	}
	
	private function printMemoryUsage()
    {
        $this->output->writeln(sprintf('Memory usage (currently) %dKB/ (max) %dKB', round(memory_get_usage(true) / 1024), memory_get_peak_usage(true) / 1024));
    }
}